#############################################################################################
#                                                                                           #
#                       Universidade Federal de Minas Gerais                                #
#                      DCC888 - Code Analysis and Optimization                              #
#                                                                                           #
#                      Project Assignment 1 - Bytecodes to dot                              #
#                                                                                           #
#                      Laura Antunã - laura.antuna@dcc.ufmg.br                              #
#                        Alex Roberto - alexrc@dcc.ufmg.br                                  #
#                                                                                           #
#############################################################################################


This is the first project from the Code Analysis and Optimization class DCC888. It consists
of a LLVM pass to convert bytecode programs into the "dot" format, generating the program's CFG.

To compile the program, simply unpack the "BytecodeToDot" directory in
"path/to/llvm/lib/Transforms", and then run the "make" command.

To run the program, first compile the desirable program using "clang"`:
----------------------------------------
$ clang -c -emit-llvm file.c -o file.bc
----------------------------------------

Then, to obtain the CFG for each one of the compiled program's functions in "dot" format, run
the following command:
-------------------------------------------------------------------------------------------
$ opt -load path/to/llvm/Debug+Asserts/lib/BytecodeToDot.so -bcToDot < file.bc > /dev/null
-------------------------------------------------------------------------------------------

Finally, to properly visualize the generated CFGs, install "dot" in your system and run
the following command:
-------------------------------
$ dot -Tpdf cfg.dot -o cfg.pdf
-------------------------------

Examples of CFG's generated for the Stanford benchmarks are available in the "\test" directory.


