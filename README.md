# DCC888 - Bytecodes to Dot

This is the first project from the Code Analysis and Optimization class ([DCC888](http://homepages.dcc.ufmg.br/~fernando/classes/dcc888)). It consists of a [LLVM](http://llvm.org/) pass to convert bytecode programs into the [dot](http://en.wikipedia.org/wiki/DOT_language) format, generating the program's CFG.

To compile the program, simply unpack the `BytecodeToDot` directory in
`path/to/llvm/lib/Transforms`, and then run the `make` command.

To run the program, first compile the desirable program using `clang`:
```bash
$ clang -c -emit-llvm file.c -o file.bc
```

Then, to obtain the CFG for each one of the compiled program's functions in ``dot`` format, run the following command:
```bash
$ opt -load path/to/llvm/Debug+Asserts/lib/BytecodeToDot.so -bcToDot < file.bc > /dev/null
```

Finally, to properly visualize the generated CFGs, install ``dot`` in your system and run the following command:
```bash
$ dot -Tpdf cfg.dot -o cfg.pdf 
```

Examples of CFG's generated for the [Stanford](https://llvm.org/viewvc/llvm-project/test-suite/trunk/SingleSource/Benchmarks/Stanford/) benchmarks are available in the ```\test``` directory.

## Credits

- Laura Antunã - laura.antuna@dcc.ufmg.br
- Alex Roberto - alexrc@dcc.ufmg.br
