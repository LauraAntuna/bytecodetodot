#include "llvm/Pass.h"
#include "llvm/Function.h"
#include <llvm-c/Core.h>
#include "llvm/Support/raw_ostream.h"
#include "llvm/Operator.h"

using namespace llvm;

namespace
{
    struct BytecodeToDot : public FunctionPass
    {
        std::map<std::string, int> basicBlockId;
        std::map<std::string, int> instructionId;
        static char ID;
        BytecodeToDot() : FunctionPass(ID) {}

        virtual bool runOnFunction(Function &F)
        {

            int bb_count = 0;
            int inst_count = 0;

            // Set output file
            std::string file_name = "cfg." + F.getName().str() + ".dot";
            std::string ErrorMsg;
            raw_fd_ostream output_file(file_name.c_str(), ErrorMsg);

            // Print CFG header
            output_file << "digraph \"CFG for \'" + F.getName() + "\' function\" {\n";

            // Iterates over basic blocks
            for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb)
            {
                // Find basic block id or map it if not found
                int bb_id;
                if (basicBlockId.find(bb->getName()) == basicBlockId.end())
                {
                    basicBlockId[bb->getName()] = bb_count;
                    bb_id = bb_count;
                    bb_count++;
                }
                else
                {
                    bb_id = basicBlockId[bb->getName()];
                }

                // Print definition of the node in dot format
                output_file << "\tBB" << bb_id
                       << " [shape=record,\n"
                       << "\t\tlabel=\"{" << bb->getName() << ":\\l\\l ";



                // Iterates over basic block instructions
                for (BasicBlock::iterator i = bb->begin(), e = bb->end();
                     i != e; ++i)
                {
                    // Check if instruction has a name and map it if positive.
                    if (i->getName() != "")
                    {
                        instructionId[i->getName()] = inst_count;
                    }

                    // Get instruction opcode
                    std::string opcode = i->getOpcodeName();

                    // Assign a name to all instructions, except for
                    // terminators instructions (except invoke) and a few
                    // memory access and addressing operations instructions
                    if ( (!i->isTerminator() || (opcode == "invoke"))
                         && (opcode != "store") && (opcode != "fence")
                         && (opcode != "cmpxchg") && (opcode != "atomicrmw") )
                    {
                        output_file << "%" << inst_count << " = ";
                        inst_count++;
                    }

                    // Print instruction opcode
                    output_file << opcode;

                    // Treat instructions accordingly to its type
                    if (OverflowingBinaryOperator *bin_op_inst = dyn_cast<OverflowingBinaryOperator>(i)) {
                        if (bin_op_inst->hasNoUnsignedWrap()) {
                            output_file << " nuw";
                        }
                        if (bin_op_inst->hasNoSignedWrap()) {
                            output_file << " nsw";
                        }
                    }
                    else if (PossiblyExactOperator *exct_op_inst = dyn_cast<PossiblyExactOperator>(i)) {
                        if (exct_op_inst->isExact()){
                            output_file << " exact";
                        }
                    }
                    else if (GEPOperator *gep_inst = dyn_cast<GEPOperator>(i)) {
                        if (gep_inst->isInBounds()) {
                            output_file << " inbounds";
                        }
                    }
                    else if (BranchInst *br_inst = dyn_cast<BranchInst>(i)) {
                        if (br_inst->isConditional()) {
                            output_file << " i1";
                        }
                    }
                    else if (LoadInst *load_inst = dyn_cast<LoadInst>(i)) {
                        if (load_inst->isAtomic()){
                            output_file << " atomic";
                        }
                        if (load_inst->isVolatile()){
                            output_file << " volatile";
                        }
                    }
                    else if (StoreInst *store_inst = dyn_cast<StoreInst>(i)) {
                        if (store_inst->isAtomic()){
                            output_file << " atomic";
                        }
                        if (store_inst->isVolatile()){
                            output_file << " volatile";
                        }
                    }
                    else if (AtomicCmpXchgInst *cmpxch_inst = dyn_cast<AtomicCmpXchgInst>(i)) {
                        if (cmpxch_inst->isVolatile()){
                            output_file << " volatile";
                        }
                    }
                    else if (AtomicRMWInst *atomicrmw_inst = dyn_cast<AtomicRMWInst>(i)) {
                        if (atomicrmw_inst->isVolatile()){
                            output_file << " volatile";
                        }
                    }
                    else if (CmpInst *cmp_inst = dyn_cast<CmpInst>(i)) {
                        output_file << " " << getPredicate(cmp_inst->getPredicate());
                    }
                    else if (CallInst *call_inst = dyn_cast<CallInst>(i)) {
                        if (call_inst->isTailCall()){
                            output_file << " tail";
                        }
                    }

                    // Iterates over instruction's operands
                    for (User::op_iterator o = i->op_begin(), e = i->op_end();
                         o != e; ++o)
                    {
                        Value *operand = o->get();

                        // Get operand name
                        if (operand->hasName()){
                            if (isa<GlobalValue> (operand)) {
                                output_file << " @";
                            }
                            else {
                                output_file << " %";
                            }

                            //  Check if operand is the result of a previous instruction
                            if (instructionId.find(operand->getName()) != instructionId.end()){
                                output_file << instructionId[operand->getName()];
                            }
                            else{
                                output_file << operand->getName();
                            }
                        }
                        // Get operand value
                        else if (ConstantInt *const_op = dyn_cast<ConstantInt>(operand)) {
                            output_file << " " << const_op->getValue();
                        }
                        else if (ConstantFP *const_fp_op = dyn_cast<ConstantFP>(operand)) {
                            bool isHalf = &const_fp_op->getValueAPF().getSemantics()==&APFloat::IEEEhalf;
                                bool isDouble = &const_fp_op->getValueAPF().getSemantics()==&APFloat::IEEEdouble;
                                bool isInf = const_fp_op->getValueAPF().isInfinity();
                                bool isNaN = const_fp_op->getValueAPF().isNaN();
                            if (!isHalf && !isInf && !isNaN) {
                                double value = isDouble ? const_fp_op->getValueAPF().convertToDouble() :
                                const_fp_op->getValueAPF().convertToFloat();
                                output_file << " " << value;
                            }
                        }
                    }
                    output_file << "\\l";

                    // Check if instruction is a terminator, to print the
                    // dot file indication of the end of a node
                    if (i->isTerminator()) {
                        output_file << "}\"];\n";
                    }
                    else {
                        output_file << " ";
                    }
                }

                // Get the basic block termination instruction and find
                // the successors of the basic block, to print the CFG edges
                TerminatorInst *term_inst = bb->getTerminator();
                unsigned num_succs = term_inst->getNumSuccessors();
                int succ_bb_id;

                for (unsigned i = 0; i < num_succs; i++)
                {
                    // Check if basic block name is already mapped
                    BasicBlock *succ = term_inst->getSuccessor(i);
                    if (basicBlockId.find(succ->getName()) == basicBlockId.end())
                    {
                        basicBlockId[succ->getName()] = bb_count;
                        succ_bb_id = bb_count;
                        bb_count++;
                    }
                    else
                    {
                        succ_bb_id = basicBlockId[succ->getName()];
                    }
                    output_file << "\tBB" << bb_id << " -> BB" << succ_bb_id << ";\n";
                }
                output_file << "\n";
            }
            output_file << "}";

            return false;
        }

        std::string getPredicate(CmpInst::Predicate predicate){
            if (predicate == CmpInst::ICMP_EQ){
                return "eq";
            }
            if (predicate == CmpInst::ICMP_NE){
                return "ne";
            }
            if (predicate == CmpInst::ICMP_UGT){
                return"ugt";
            }
            if (predicate == CmpInst::ICMP_UGE){
                return"uge";
            }
            if (predicate == CmpInst::ICMP_ULT){
                return"ult";
            }
            if (predicate == CmpInst::ICMP_SGT){
                return"sgt";
            }
            if (predicate == CmpInst::ICMP_SGE){
                return"sge";
            }
            if (predicate == CmpInst::ICMP_SLT){
                return"slt";
            }
            if (predicate == CmpInst::ICMP_SLE){
                return"sle";
            }
            if (predicate == CmpInst::FCMP_FALSE){
                return"false";
            }
            if (predicate == CmpInst::FCMP_OEQ){
                return"oeq";
            }
            if (predicate == CmpInst::FCMP_OGT){
                return"ogt";
            }
            if (predicate == CmpInst::FCMP_OGE){
                return"oge";
            }
            if (predicate == CmpInst::FCMP_OLT){
                return"olt";
            }
            if (predicate == CmpInst::FCMP_OLE){
                return"ole";
            }
            if (predicate == CmpInst::FCMP_ONE){
                return"one";
            }
            if (predicate == CmpInst::FCMP_ORD){
                return"ord";
            }
            if (predicate == CmpInst::FCMP_UNO){
                return"uno";
            }
            if (predicate == CmpInst::FCMP_UEQ){
                return"ueq";
            }
            if (predicate == CmpInst::FCMP_UGT){
                return"egt";
            }
            if (predicate == CmpInst::FCMP_UGE){
                return"ege";
            }
            if (predicate == CmpInst::FCMP_ULT){
                return"ult";
            }
            if (predicate == CmpInst::FCMP_ULE){
                return"ule";
            }
            if (predicate == CmpInst::FCMP_UNE){
                return"une";
            }
            if (predicate == CmpInst::FCMP_TRUE){
                return"true";
            }
            return "";
        }
    };
}
char BytecodeToDot::ID = 0;
static RegisterPass<BytecodeToDot> X("bcToDot", "Prints bytecode programs into the dot format", false, false);
